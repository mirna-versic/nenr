# Neizrazito, evolucijsko i neuro racunarstvo

Implementacija treceg laboratorijskog zadatka.

Tema: Ucenje unaprijedne slojevite potpuno povezane neuronske mreze

Algoritmi: 

	- Algoritam propagacije pogreske unatrag (Backpropagation algorithm)
	- Stohasticki algoritam propagacije pogreske unatrag (Stohastic backpropagation algorithm)
	- Algoritam propagacije pogreske unatrag za male grupe (Mini-batch backpropagation algorithm) 



