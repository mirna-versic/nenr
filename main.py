from GUI import app_starter
from Algorithms import data_processing
from Algorithms import config


def main() -> None:
    #examples: dict = app_starter.app()
    #data_processing.DataProcessing(examples)
    X, Y = get_data()


def get_data() -> tuple:
    file = open(config.get_path_to_data(), "r+")
    X: list = []
    Y: list = []

    lines: list = file.readlines()
    for line in lines:
        line = line.strip().replace(",", "").split(" ")
        X.append(list(map(float, line[:-1])))
        Y = line[-1]

    return X, Y


if __name__ == "__main__":
    main()
