from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import sys


class Window(QMainWindow):

    def __init__(self):
        super().__init__()
        self.initialize_window()
        self.image = self.initialize_image()
        self.file_menu = self.initialize_main_menu()
        self.brush_size = 2
        self.brush_colour = Qt.black
        self.last_point = QPoint()
        self.drawing_active = False
        self.pixels = []
        self.examples = {}

        self.actions = []
        self.example_menus = []
        self.i = 0

    def initialize_window(self) -> None:
        self.setWindowTitle("Simple paint app")
        self.setGeometry(100, 100, 800, 600)

    def initialize_image(self) -> QImage:
        image = QImage(self.size(), QImage.Format_RGB32)
        image.fill(Qt.white)
        return image

    def initialize_main_menu(self):
        main_menu = self.menuBar()
        file_menu = main_menu.addMenu("File")
        return file_menu

    def mousePressEvent(self, event) -> None:
        if event.button() == Qt.LeftButton:
            self.drawing_active = True
            self.last_point = event.pos()
            self.pixels.append([self.last_point.x(), self.last_point.y()])
            self.clear()

    def mouseMoveEvent(self, event) -> None:
        if (event.buttons() & QtCore.Qt.LeftButton) and self.drawing_active:
            painter = QPainter(self.image)
            painter.setPen(QPen(self.brush_colour, self.brush_size, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
            painter.drawLine(self.last_point, event.pos())

            self.last_point = event.pos()
            self.pixels.append([self.last_point.x(), self.last_point.y()])
            self.update()

    def mouseReleaseEvent(self, event) -> None:
        if event.button() == Qt.LeftButton:
            self.drawing_active = False

            self.examples[self.i] = self.pixels
            example_menu = self.file_menu.addMenu("Example " + str(self.i))
            action = QAction("Delete")
            example_menu.addAction(action)

            self.actions.append(action)
            self.example_menus.append(example_menu)

            self.update()
            self.pixels = []
            self.i += 1

            for i in range(len(self.actions)):
                action.triggered.disconnect()
                action.triggered.connect(lambda: self.delete(i, self.example_menus[i]))

    def delete(self, i, example_menu) -> None:
        example_menu.menuAction().setVisible(False)
        del self.examples[i]
        self.update()

    def paintEvent(self, event) -> None:
        canvas_painter = QPainter(self)
        canvas_painter.drawImage(self.rect(), self.image, self.image.rect())

    def clear(self) -> None:
        self.image.fill(Qt.white)
        self.update()


def app() -> dict:
    paint_app = QApplication(sys.argv)

    window = Window()
    window.show()
    paint_app.exec()
    return window.examples
