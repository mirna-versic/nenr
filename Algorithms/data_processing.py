from Algorithms import config
from typing import Tuple
from math import sqrt


class DataProcessing:

    def __init__(self, examples: dict) -> None:
        self.path_to_data: str = config.get_path_to_data()
        self.processed_data = self.process_data(examples)
        self.write_to_file(self.processed_data)

    def write_to_file(self, data: dict) -> None:
        file = open("examples.txt", "w+")

        keys = data.keys()
        for key in keys:
            letters = data[key]
            for letter in letters:
                for pixel in letter:
                    file.write(str(pixel[0]) + " " + str(pixel[1]) + ", ")
                file.write(key + "\n")

    def process_data(self, data: dict) -> dict:
        average_data = self.count_average_place(data)
        average_data = self.count_average_szie(average_data)
        return self.get_features(average_data)

    def count_average_place(self, data: dict) -> dict:
        keys = list(data.keys())
        keys.sort()

        for key in keys:
            pixels = data[key]
            average_x = 0.0
            average_y = 0.0

            for pixel in pixels:
                average_x += pixel[0]
                average_y += pixel[1]

            average_x /= len(pixels)
            average_y /= len(pixels)

            for pixel in pixels:
                pixel[0] -= average_x
                pixel[1] -= average_y
        return data

    def get_max_x_and_y(self, pixels: list) -> Tuple[float, float]:
        max_x = 0.0
        max_y = 0.0
        for pixel in pixels:
            if abs(pixel[0]) > max_x:
                max_x = abs(pixel[0])
            if abs(pixel[1]) > max_y:
                max_y = abs(pixel[1])

        return max_x, max_y

    def count_average_szie(self, data: dict) -> dict:
        keys = list(data.keys())
        keys.sort()

        for key in keys:
            pixels = data[key]
            max_x, max_y = self.get_max_x_and_y(pixels)
            m = max(max_x, max_y)
            for pixel in pixels:
                pixel[0] /= m
                pixel[1] /= m

        return data

    def get_features(self, data: dict) -> dict:
        final_list = []
        return_value = {}
        keys = list(data.keys())
        keys.sort()

        k = 0
        j = 0
        for key in keys:
            d = self.get_distance(data[key])
            pixels = data[key]
            tmp_list = []

            for i in range(0, config.get_m()):
                distance = i * d / (config.get_m() - 1)
                pixel = self.get_closest_pixel(distance, pixels)
                tmp_list.append(pixel)

            final_list.append(tmp_list)
            k += 1

            if k >= 5:
                return_value[config.get_expected_output()[j]] = final_list
                final_list = []
                k = 0
                j += 1

        return return_value

    def get_closest_pixel(self, distance: float, pixels: list) -> list:
        nearest_pixel = None
        min_distance_found = None

        tmp_distance = 0.0
        for i in range(1, len(pixels)):
            tmp_distance += sqrt((pixels[i][0] - pixels[i - 1][0]) ** 2 + (pixels[i][1] - pixels[i - 1][1]) ** 2)

            if min_distance_found is None or abs(tmp_distance - distance) < min_distance_found:
                min_distance_found = abs(tmp_distance - distance)
                nearest_pixel = pixels[i]

        return nearest_pixel

    def get_distance(self, pixels: list) -> float:
        d = 0.0
        for i in range(1, len(pixels)):
            d += sqrt((pixels[i][0] - pixels[i - 1][0]) ** 2 + (pixels[i][1] - pixels[i - 1][1]) ** 2)

        return d
