path_to_data = "examples.txt"
m = 10
output = {0: "1,0,0,0,0",
          1: "0,1,0,0,0",
          2: "0,0,1,0,0",
          3: "0,0,0,1,0",
          4: "0,0,0,0,1"}


def get_path_to_data() -> str:
    return path_to_data


def get_m() -> int:
    return m

def get_expected_output() -> dict:
    return output
